/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Motion
 * @{
 *
 * @brief 手势识别设备驱动对硬件服务提供通用的接口能力。
 *
 * 模块提供硬件服务对手势识别驱动模块访问统一接口，服务获取驱动对象或者代理后，通过其提供的各类方法，实现使能手势识别/
 * 去使能手势识别、订阅/去订阅手势识别数据。
 *
 * @since 3.2
 */

/**
 * @file IMotionCallback.idl
 *
 * @brief 定义上报手势识别数据回调函数接口。
 *
 * 在手势识别用户订阅手势识别数据时需要注册这个回调函数接口的实例。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief 手势识别模块接口的包路径。
 *
 * @since 3.2
 */
package ohos.hdi.motion.v1_0;

import ohos.hdi.motion.v1_0.MotionTypes;

/**
 * @brief 定义上报手势识别数据回调函数。
 *
 * 手势识别用户在订阅手势识别数据时需要注册这个回调函数，只有当使能手势识别后，手势识别数据用户才会收到手势识别数据。
 * 详情可参考{@link IMotionInterface}。
 *
 * @since 3.2
 */
[callback] interface IMotionCallback {
    /**
    * @brief 定义上报手势识别数据回调函数。
    *
    * @param event 回调函数上报的数据。详见{@link HdfMotionEvent}。
    *
    * @return 如果回调函数上报数据成功，则返回0。
    * @return 如果回调函数上报数据失败，则返回负值。
    *
    * @since 3.2
    */
    OnDataEvent([in] struct HdfMotionEvent event);
}
/** @} */