/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Vibrator
 * @{
 *
 * @brief 马达驱动对马达服务提供通用的接口能力。
 *
 * 模块提供马达服务对马达驱动访问的统一接口，服务获取驱动对象或者代理后，控制马达的单次振动、周期性振动、停止振动。
 *
 * @since 2.2
 * version 1.0
 */

/**
 * @file VibratorTypes.idl
 *
 * @brief 定义马达数据结构，包括马达振动模式。
 *
 * @since 2.2
 * @version 1.0
 */

/**
 * @brief 马达模块接口的包路径。
 *
 * @since 2.2
 * @version 1.0
 */
package ohos.hdi.vibrator.v1_0;

/**
 * @brief 定义马达振动模式。
 *
 * @since 2.2
 * @version 1.0
 */
enum HdfVibratorMode {
    HDF_VIBRATOR_MODE_ONCE,     /**< 表示给定持续时间内的单次振动。 */
    HDF_VIBRATOR_MODE_PRESET,   /**< 表示具有预置效果的周期性振动。 */
    HDF_VIBRATOR_MODE_BUTT,     /**< 表示效果模式无效。 */
};
/** @} */
