/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TYPES_H
#define C_INCLUDE_DRAWING_TYPES_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数
 * 
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_types.h
 *
 * @brief 文件中定义了用于绘制2d图形的数据类型，包括画布、画笔、画刷、位图和路径
 *
 * @since 8
 * @version 1.0
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief OH_Drawing_Canvas定义为一块矩形的画布，可以结合画笔和画刷在上面绘制各种形状、图片和文字
 * 
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Canvas OH_Drawing_Canvas;

/**
 * @brief OH_Drawing_Pen定义为画笔，画笔用于描述绘制图形轮廓的样式和颜色
 * 
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Pen OH_Drawing_Pen;

/**
 * @brief OH_Drawing_Brush定义为画刷，画刷用于描述填充图形的样式和颜色
 * 
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Brush OH_Drawing_Brush;

/**
 * @brief OH_Drawing_Path定义为路径，路径用于自定义各种形状
 * 
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Path OH_Drawing_Path;

/**
 * @brief OH_Drawing_Bitmap定义为位图，位图是一块内存，内存中包含了描述一张图片的像素数据
 * 
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Bitmap OH_Drawing_Bitmap;

/**
 * @brief OH_Drawing_ColorFormat用于描述位图像素的存储格式
 * 
 * @since 8
 * @version 1.0
 */
typedef enum {
    /** 未知格式. */
    COLOR_FORMAT_UNKNOWN,
    /** 每个像素用一个8位的量表示，8个位比特位表示透明度 */
    COLOR_FORMAT_ALPHA_8,
    /** 每个像素用一个16位的量表示，高位到低位依次是5个比特位表示红，6个比特位表示绿，5个比特位表示蓝 */
    COLOR_FORMAT_RGB_565,
    /** 每个像素用一个16位的量表示，高位到低位依次是4个比特位表示透明度，4个比特位表示红，4个比特位表示绿，4个比特位表示蓝 */
    COLOR_FORMAT_ARGB_4444,
    /** 每个像素用一个32位的量表示，高位到低位依次是8个比特位表示透明度，8个比特位表示红，8个比特位表示绿，8个比特位表示蓝 */
    COLOR_FORMAT_RGBA_8888,
    /** 每个像素用一个32位的量表示，高位到低位依次是8个比特位表示蓝，8个比特位表示绿，8个比特位表示红，8个比特位表示透明度 */
    COLOR_FORMAT_BGRA_8888
} OH_Drawing_ColorFormat;

/**
 * @brief OH_Drawing_AlphaFormat用于描述位图像素的透明度分量
 * 
 * @since 8
 * @version 1.0
 */
typedef enum {
    /** 未知格式 */
    ALPHA_FORMAT_UNKNOWN,
    /** 位图无透明度 */
    ALPHA_FORMAT_OPAQUE,
    /** 每个像素的颜色组件由透明度分量预先乘以 */
    ALPHA_FORMAT_PREMUL,
    /** 每个像素的颜色组件未由透明度分量预先乘以 */
    ALPHA_FORMAT_UNPREMUL
} OH_Drawing_AlphaFormat;

#ifdef __cplusplus
}
#endif
/** @} */
#endif