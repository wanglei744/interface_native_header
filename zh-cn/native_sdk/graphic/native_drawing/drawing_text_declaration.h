/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TEXT_DECLARATION_H
#define C_INCLUDE_DRAWING_TEXT_DECLARATION_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief 提供2D绘制功能
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_text_declaration.h
 *
 * @brief 提供2d drawing文本相关的数据结构声明
 *
 * @since 8
 * @version 1.0
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief OH_Drawing_FontCollection用于加载字体
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_FontCollection OH_Drawing_FontCollection;

/**
 * @brief OH_Drawing_Typography用于管理排版的布局和显示等
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Typography OH_Drawing_Typography;

/**
 * @brief OH_Drawing_TextStyle用于管理字体颜色、装饰等
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_TextStyle OH_Drawing_TextStyle;

/**
 * @brief OH_Drawing_TypographyStyle用于管理排版风格，如文字方向等
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_TypographyStyle OH_Drawing_TypographyStyle;

/**
 * @brief OH_Drawing_TypographyCreate用于创建OH_Drawing_Typography
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_TypographyCreate OH_Drawing_TypographyCreate;

#ifdef __cplusplus
}
#endif
/** @} */
#endif