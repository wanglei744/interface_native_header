/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** 
 * @file metanode_demo.c
 * 
 * @brief 元节点的demo演示
 * 
 * @since 1.0
 * @version 1.0
 */

#include "softbus_bus_center.h"
#include "softbus_common.h"
#include <stdint.h>

int main(void)
{
    const char *pkgName = "pkgName.demo";
    char udid[] = "0123456789987654321001234567899876543210012345678998765432100123";
    char metaNodeId[NETWORK_ID_BUF_LEN] = {0};
    MetaNodeInfo infos[MAX_META_NODE_NUM];
    int32_t infoNum = MAX_META_NODE_NUM;
    MetaNodeConfigInfo configInfo;
    configInfo.addrNum = 1;

    if (strncpy_s(configInfo.udid, UDID_BUF_LEN, udid, UDID_BUF_LEN) != 0) {
        printf("udid copy fail\n");
        return -1;
    }

    // 元节点激活
    if (ActiveMetaNode(pkgName, &configInfo, metaNodeId) != 0) {
        printf("[demo] ActiveMetaNode fail\n");
        return -1;
    }

    // 获取所有元节点信息
    if (GetAllMetaNodeInfo(pkgName, infos, &infoNum) != 0) {
        printf("[demo] GetAllMetaNodeInfo fail\n");
        return -1;
    }

    // 释放元节点
    if (DeactiveMetaNode(pkgName, metaNodeId) != 0) {
        printf("DeactiveMetaNode fail\n");
        return -1;
    }
    return 0;
}