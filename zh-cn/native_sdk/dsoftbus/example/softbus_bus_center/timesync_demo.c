/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** 
 * @file timesync_demo.c
 * 
 * @brief 时间同步的demo演示
 * 
 * @since 1.0
 * @version 1.0
 */

#include "softbus_bus_center.h"
#include "softbus_common.h"
#include <stdint.h>

static void OnTimeSyncResult(const TimeSyncResultInfo *info, int32_t retCode)
{
    (void)info;
    (void)retCode;
}

static ITimeSyncCb g_timeSyncCb = {
    .onTimeSyncResult = OnTimeSyncResult,
};

int main(void)
{
    const char *pkgName = "pkgName.demo";
    char networkId[] = "0123456789987654321001234567899876543210012345678998765432100123";
    TimeSyncAccuracy timeAccuracy = SUPER_HIGH_ACCURACY;
    TimeSyncPeriod period = NORMAL_PERIOD;
    
    // 时间同步
    if (StartTimeSync(pkgName, networkId, timeAccuracy, period, &g_timeSyncCb) != 0) {
        printf("[demo] StartTimeSync fail\n");
        return -1;
    }

    // 停止时间同步
    if (StopTimeSync(pkgName, networkId) != 0) {
        printf("[demo] StopTimeSync fail\n");
        return -1;
    }
    return 0;  
}