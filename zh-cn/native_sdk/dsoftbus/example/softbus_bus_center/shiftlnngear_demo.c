/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** 
 * @file shiftlnngear_demo.c
 * 
 * @brief 调整心跳的demo演示
 * 
 * @since 1.0
 * @version 1.0
 */

#include "softbus_bus_center.h"
#include "softbus_common.h"
#include <stdint.h>

int main(void)
{
    const char *pkgName = "pkgName.demo";
    const char *callerId = "1";
    const char *networkId = NULL;
    GearMode mode = {
        .cycle = MID_FREQ_CYCLE,
        .duration = DEFAULT_DURATION,
        .wakeupFlag = false,
    };
    
    // 调整心跳
    if (ShiftLNNGear(pkgName, callerId, networkId, &mode) != 0) {
        printf("ShiftLNNGear fail\n");
        return -1;
    }
    return 0;
}