/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** 
 * @file devicepublish_demo.c
 * 
 * @brief 设备发布服务的demo演示
 * 
 * @since 1.0
 * @version 1.0
 */

#include "discovery_service.h"
#include "softbus_bus_center.h"
#include "softbus_common.h"
#include <stdint.h>


static void TestPublishResult(int publishId, PublishResult reason)
{
    return;
}

static IPublishCb g_publishCb = {
    .OnPublishResult = TestPublishResult,
};

static PublishInfo g_pInfo = {
    .publishId = 1,                                 /* 发布ID */
    .mode = DISCOVER_MODE_ACTIVE,                   /* 发布选择主动发布 */
    .medium = COAP,                                 /* 发布媒介：COAP发布 */
    .freq = MID,                                    /* 发布频次：中等 */
    .capability = "dvKit",                          /* 发布能力 */
    .capabilityData = (unsigned char *)"capdata4",  /* 发布能力数据 */
    .dataLen = sizeof("capdata4")                   /* 发布能力数据长长度 */
};

static SubscribeInfo g_sInfo1 = {
    .subscribeId = 1,
    .medium = COAP,
    .mode = DISCOVER_MODE_PASSIVE,
    .freq = MID,
    .capability = "ddmpCapability",
    .capabilityData = (unsigned char *)"capdata3",
    .dataLen = sizeof("capdata3"),
    .isSameAccount = true,
    .isWakeRemote = false
};

static void TestDeviceFound(const DeviceInfo *device)
{
    printf("[demo]TestDeviceFound\n");
}

static void TestDiscoverFailed(int subscribeId, DiscoveryFailReason failReason)
{
    printf("[demo]TestDiscoverFailed, subscribeId = %d, failReason = %d\n", subscribeId, failReason);
}

static void TestDiscoverySuccess(int subscribeId)
{
    printf("[demo]TestDiscoverySuccess, subscribeId = %d\n", subscribeId);

static IDiscoveryCallback g_subscribeCb = {
    .OnDeviceFound = TestDeviceFound,
    .OnDiscoverFailed = TestDiscoverFailed,
    .OnDiscoverySuccess = TestDiscoverySuccess
};

/* 场景：设备A为发现方，设备B为被发现方，
 * 设备B启动后调用PublishLNN服务发布。
 * 设备A通过调用StartDiscovery触发监听
 */

/* 设备A通过调用StartDiscovery触发广播监听，发现周围设备 */
static void DeviceAStartDiscovery(void)
{
    const char *pkgNameA = "pkgName.demo1";
    (void)StartDiscovery(pkgNameA, &g_sInfo1, &g_subscribeCb);
}

/* 设备B通过调用PublishLNN进行广播 */
int main(void)
{
    const char *pkgName = "pkgName.demo";
    if (PublishLNN(pkgName, &g_pInfo, &g_publishCb) != 0) {
        printf("[demo] PublishLNN fail\n");
        return -1;
    }
    return 0;
}
