/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** 
 * @file deviceleavelnn_demo.c
 * 
 * @brief 设备退网的demo演示
 * 
 * @since 1.0
 * @version 1.0
 */

#include "softbus_bus_center.h"
#include "softbus_common.h"
#include <stdint.h>


static void OnLeaveLNNDone(const char *networkId, int32_t retCode)
{
    (void)networkId;
    (void)retCode;
}

// 设备A
static int DeviceAMain(void)
{
    const char *pkgName = "pkgName.demo";
    char networkId[] = "0123456789987654321001234567899876543210012345678998765432100123";
    // 1.设备A调用LeaveLNN退网 
    if (LeaveLNN(pkgName, networkId, OnLeaveLNNDone) != 0) {
        printf("[demo]LeaveLNN error!");
        return -1;
    }
    // 2.设备A调用CompleteLeaveLNN 完成退网通知
    return 0;
}

// 设备B
static int DeviceBMain(void)
{
    // 1.设备A调用LeaveLNN退网之后，通过状态机的判断，设备B会执行CompleteLeaveLNN
}