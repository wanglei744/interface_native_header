/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** 
 * @file devicejoinlnn_demo.c
 * 
 * @brief 设备组网的demo演示
 * 
 * @since 1.0
 * @version 1.0
 */

#include "softbus_bus_center.h"
#include "softbus_common.h"
#include <stdint.h>

// 设备A和设备B无可信关系
// 设备A向软总线注册组网回调 
static void OnJoinLNNDone(const ConnectionAddr *addr, const char *networkId, int retCode)
{
    if (addr == NULL) {
        printf("[demo]OnJoinLNNDone error\n");
        return;
    }
    if (retCode == 0) {
        printf("[demo]OnJoinLNNDone enter networdid = %s, retCode = %d ip = %s port = %d\r\n",
            networkId, retCode, addr->info.ip.ip, addr->info.ip.port);
    } else {
        printf("[demo]OnJoinLNNDone failed! networdid = %s, retCode = %d\r\n", networkId, retCode);
    }
}

// 设备A调用JoinLNN接口入网 
static int DeviceAMain(void)
{
    const char *pkgName = "pkgName.demo";
    ConnectionAddr addr = {
        .type = CONNECTION_ADDR_WLAN,
        .peerUid = "0123456789987654321001234567899876543210012345678998765432100123",
        .info.ip.ip = "192.168.0.1",
        .info.ip.port = 1000,
    };
    const char *sessionName = "sessionName.demo";
    // 1.设备A会根据会话名称和连接信息在组网前打开认证通道进行身份协商
    int sessionId = OpenAuthSession(sessionName, &addr, 1, NULL);

    // 2.当成功打开身份认证会话时，通过NotifyAuthSuccess 通知设备认证成功
    NotifyAuthSuccess(sessionId);

    // 3.认证成功后，设备A调用JoinLNN入网，通过OnJoinLNNDone来返回调用接口的结果
    if (JoinLNN(pkgName, &addr, OnJoinLNNDone) != 0) {
        printf("[demo]JoinLNN error!");
        return -1;
    }
    return 0;
    // 4.会调用CompleteJoinLNN 完成上线通知
}

// 设备B
static int DeviceBMain(void)
{
    // 1.设备B 在认证通道打开之前，会打开AuthPort，来建立认证通道。
    // 2.设备B 同样会调用CompleteJoinLNN，完成组网上报通知。
}