/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef MINDSPORE_INCLUDE_C_API_FORMAT_C_H
#define MINDSPORE_INCLUDE_C_API_FORMAT_C_H

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief  MSTensor保存的数据支持的排列格式。
 * 
 * @since 9
 */
typedef enum OH_AI_Format {
  /** 表示NCHW排列 */
  OH_AI_FORMAT_NCHW = 0,
  /** 表示NHWC排列 */
  OH_AI_FORMAT_NHWC = 1,
  /** 表示NHWC4排列 */
  OH_AI_FORMAT_NHWC4 = 2,
  /** 表示HWKC排列 */
  OH_AI_FORMAT_HWKC = 3,
  /** 表示HWCK排列 */
  OH_AI_FORMAT_HWCK = 4,
  /** 表示KCHW排列 */
  OH_AI_FORMAT_KCHW = 5,
  /** 表示CKHW排列 */
  OH_AI_FORMAT_CKHW = 6,
  /** 表示KHWC排列 */
  OH_AI_FORMAT_KHWC = 7,
  /** 表示CHWK排列 */
  OH_AI_FORMAT_CHWK = 8,
  /** 表示HW排列 */
  OH_AI_FORMAT_HW = 9,
  /** 表示HW4排列 */
  OH_AI_FORMAT_HW4 = 10,
  /** 表示NC排列 */
  OH_AI_FORMAT_NC = 11,
  /** 表示NC4排列 */
  OH_AI_FORMAT_NC4 = 12,
  /** 表示NC4HW4排列 */
  OH_AI_FORMAT_NC4HW4 = 13,
  /** 表示NCDHW排列 */
  OH_AI_FORMAT_NCDHW = 15,
  /** 表示NWC排列 */
  OH_AI_FORMAT_NWC = 16,
  /** 表示NCW排列 */
  OH_AI_FORMAT_NCW = 17
} OH_AI_Format;

#ifdef __cplusplus
}
#endif
#endif  // MINDSPORE_INCLUDE_C_API_FORMAT_C_H
