/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_AVSESSION_ERRORS_H
#define OHOS_AVSESSION_ERRORS_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief 音视频媒体会话，提供系统内媒体的统一控制能力。
 *
 * 功能包括媒体会话，媒体会话管理，媒体会话控制。
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_errors.h
 *
 * @brief 定义了avsession错误码。
 *
 * @since 9
 * @version 1.0
 */

#include <cinttypes>
#include "errors.h"

namespace OHOS::AVSession {
/** 存在错误 */
constexpr int32_t  AVSESSION_ERROR = -1;
/** 操作成功 */
constexpr int32_t  AVSESSION_SUCCESS = 0;
/** avsession错误码的基定义 */
constexpr int32_t  AVSESSION_ERROR_BASE = 1000;
/** 无可用内存 */
constexpr int32_t  ERR_NO_MEMORY = -(AVSESSION_ERROR_BASE + 1);
/** 传递的参数无效 */
constexpr int32_t  ERR_INVALID_PARAM = -(AVSESSION_ERROR_BASE + 2);
/** 服务不存在 */
constexpr int32_t  ERR_SERVICE_NOT_EXIST = -(AVSESSION_ERROR_BASE + 3);
/** Session监听器已存在 */
constexpr int32_t  ERR_SESSION_LISTENER_EXIST = -(AVSESSION_ERROR_BASE + 4);
/** 数据序列化操作错误 */
constexpr int32_t  ERR_MARSHALLING = -(AVSESSION_ERROR_BASE + 5);
/** 数据反序列化操作错误 */
constexpr int32_t  ERR_UNMARSHALLING = -(AVSESSION_ERROR_BASE + 6);
/** IPC发送数据失败 */
constexpr int32_t  ERR_IPC_SEND_REQUEST = -(AVSESSION_ERROR_BASE + 7);
/** 超过允许会话最大数量 */
constexpr int32_t  ERR_SESSION_EXCEED_MAX = -(AVSESSION_ERROR_BASE + 8);
/** 会话不存在 */
constexpr int32_t  ERR_SESSION_NOT_EXIST = -(AVSESSION_ERROR_BASE + 9);
/** 会话命令不支持 */
constexpr int32_t  ERR_COMMAND_NOT_SUPPORT = -(AVSESSION_ERROR_BASE + 10);
/** 控制器不存在 */
constexpr int32_t  ERR_CONTROLLER_NOT_EXIST = -(AVSESSION_ERROR_BASE + 11);
/** 无权限 */
constexpr int32_t  ERR_NO_PERMISSION = -(AVSESSION_ERROR_BASE + 12);
/** 会话未激活 */
constexpr int32_t  ERR_SESSION_DEACTIVE = -(AVSESSION_ERROR_BASE + 13);
/** 控制器存在 */
constexpr int32_t  ERR_CONTROLLER_IS_EXIST = -(AVSESSION_ERROR_BASE + 14);
/** 元能力正在运行 */
constexpr int32_t  ERR_START_ABILITY_IS_RUNNING = -(AVSESSION_ERROR_BASE + 15);
/** 元能力启动超失败 */
constexpr int32_t  ERR_ABILITY_NOT_AVALIABLE = -(AVSESSION_ERROR_BASE + 16);
/** 元能力启动超时 */
constexpr int32_t  ERR_START_ABILITY_TIMEOUT = -(AVSESSION_ERROR_BASE + 17);
/** 指令发送次数超过最大值 */
constexpr int32_t ERR_COMMAND_SEND_EXCEED_MAX = -(AVSESSION_ERROR_BASE + 18);
/** RPC发送数据失败 */
constexpr int32_t  ERR_RPC_SEND_REQUEST = -(AVSESSION_ERROR_BASE + 19);
}  // namespace OHOS::AVSession
/** @} */
#endif  // OHOS_AVSESSION_ERRORS_H